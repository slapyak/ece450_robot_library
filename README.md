# README #

### What is this repository for? ###

* Repository is for all participants in the GMU ECE-450 summer session. The intention is to allow all team members access to the Robot Library developed by David Wernli for use in the semester project. 
* 0.1

### How do I get set up? ###

* To use this library - Download the zipped file. Refer to here for instruction on how to proceed from there : http://arduino.cc/en/Guide/Libraries

### Contribution guidelines ###

* All edits shall be submitted using GitHub or Bitbucket Git management sites. 

### Who do I talk to? ###

* David Wernli - dwernli2 @ [our university email]
* Submit any question or issues to the bug tracker here on BitBucket.