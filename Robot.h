/*
  Robot.h - Library for GMU ECE450 Stingray Robot control
  Created by David R. Wernli, June 18,2014
*/
#ifndef Robot_h
#define Robot_h
#include "Arduino.h"
//#include "Waypoint.h"
//#include <SoftwareSerial.h>
//#include <TinyGPS.h>

static int _Speed;

class Robot
{
  public:
    Robot();
    //setup functions
    void start();
    void setLeft(int enPin, int hPin1, int hPin2);
    void setRight(int enPin, int hPin1, int hPin2);
    void swapWheels();
    void swapDirectionPins();
    void swapDirectionPins(char wheel[]);
    void setWheelRadius(int radius);
    //drive functions
    void setSpeed(int speed);
    void driveForward(int speed=-1); //speed is optional
    void driveReverse(int speed=-1); //speed is optional
    void drive(int speed_l=-1, int speed_r = -1, int dir=1);
    void drive_dif(int diff, int speed=-1, int MAX_DIFF = 50); //speed is optional
    void stop();
    void coast();
    int  pivot_ang(int angle);    //returns 1 when completed
    void pivot(int dir=1, int speed=-1, int type=0);
    int  turn_ang(int angle, int INspeed=-1);    //returns 1 when completed
    void turn(int dir);
    //sensor functions
    int ping(int pingPin);    //returns distance to pingback in cm
    float IRdistance(int IRpin);
    int IRdistance_mm(int IRpin);   //distance returned in mm for enhanced precision
    //GPS Functions
//    void GPStest();         //runs a test scipt reporting current position
//    void getPos();          //gets a new fix
//    int  angle_to(Waypoint dest);   //calculates angle from current heading to destination
//    void turn_toward(Waypoint dest, int err);   //not yet used
//    float dist_to(Waypoint destination);        //returns distance to destination in meters
//    int  travel_to(Waypoint destination, int tolerance = 1);    
        //travels to within tolerance meters of destination point
//    void speed_check();     //compares the dead reckoning speed with GPS speed
  private:
    void _go(int left, int right); //both arguments are optional - default to prior speed
    void setDirection(int dir);
    void setDirection(int right, int left);    
    void _pivotLeft(int dir=1, int speed=-1);
    void _pivotRight(int dir=1, int speed=-1);
    void _pivotCenter(int dir=1, int speed=-1);
    int  _abs(int value);
    //private variables
    int _mLpinEN;  // H-Bridge enable pin, Left motor
    int _mLpin1;   // H-Bridge input pins
    int _mLpin2;
    int _mRpinEN;  // H-Bridge enable pin, Right motor
    int _mRpin1;   // H-Bridge input pins
    int _mRpin2;
    int _alive;
    //private GPS variables
//    Waypoint _curr_pos;
//    unsigned long _fix_age;    
};

#endif